package com.company;

public class GameObject {
    private char symbol;
    private int x;
    private int y;

    //Constructor
    public GameObject(char symbol, int x, int y) {
        setSymbol(symbol);
        setX(x);
        setY(y);
    }

    //Getters
    public char getSymbol() {
        return symbol;
    }
    public int getY() {
        return y;
    }
    public int getX() {
        return x;
    }

    //Setters
    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }
    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
}
