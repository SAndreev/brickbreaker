package com.company;

import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.swing.SwingTerminal;

import java.io.File;
import static com.googlecode.lanterna.terminal.Terminal.Color.*;

public class Game {
    //Game screen dimensions
    private final int SCREEN_HEIGHT = 30;
    private final int SCREEN_WIDTH = 10;
    //Game field txt file
    public static final String ABSOLUTE_PATH = System.getProperty("user.dir");
    private static final String GAME_FIELD_FILE_NAME = "\\src\\com\\company\\gamefield.txt";
    public static final String GAME_FIELD_FILE_DIRECTORY = ABSOLUTE_PATH + GAME_FIELD_FILE_NAME;

    private Screen screen;
    private SwingTerminal terminal;
    private GameField gameField;

    //Constructor
    public Game() {
        setTerminal();
        setScreen();
        setGameField();
    }

    public static void main(String[] args) {
        Game game = new Game();
    }

    //Getters
    public Screen getScreen() {
        return screen;
    }
    public SwingTerminal getTerminal() {
        return terminal;
    }
    public GameField getGameField() {
        return gameField;
    }

    //Setters
    public void setScreen() {
        this.screen = new Screen(terminal);
        screen.setCursorPosition(null);
        screen.startScreen();
    }
    public void setTerminal() {
        this.terminal = new SwingTerminal(SCREEN_HEIGHT, SCREEN_WIDTH);
        terminal.applyBackgroundColor(BLACK);
    }
    public void setGameField() {
        this.gameField = new GameField(new File(GAME_FIELD_FILE_DIRECTORY));
    }
}
