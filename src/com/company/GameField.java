package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GameField {
    private GameObject[][] field;

    //Constructor
    public GameField(File file) {
        setField(file);
    }

    //Getters
    public GameObject[][] getField() {
        return field;
    }

    //Setters
    public void setField(File file) {
        this.field = readGameFieldFromFile(file);
    }


    private static GameObject[][] readGameFieldFromFile(File file){
        try {
            Scanner fileReader = new Scanner(file);

            //Return null if the file is empty
            if(file.length() == 0){
                System.out.println("File is empty!");
                return null;
            }

            //Reading game field dimensions
            int length = Integer.parseInt(fileReader.nextLine());
            int width = Integer.parseInt(fileReader.nextLine());
            GameObject[][] field = new GameObject[length][width];

            //Reading game field symbols
            int currentRow = 0;
            while (fileReader.hasNextLine()) {
                String line = fileReader.nextLine();
                for (int i = 0; i < line.length(); i++) {
                    char currentSymbol = line.charAt(i);
                    GameObject currentObject = new GameObject(currentSymbol, i, currentRow);
                    field[currentRow][i] = currentObject;
                }
                currentRow++;
            }

            return field;
        } catch (FileNotFoundException e) {
            System.out.println("Invalid file!");
            e.printStackTrace();
            return null;
        }
    }
}
